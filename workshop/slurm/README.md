Slurm MPI / OpenMP example
======

In this exercise we learn how to submit a job to the slurm queue system.
We will see how the `srun` command will handle resources for mpi jobs.

1) **Compile the small program**

   Open the test program `main.c` in your favorite editor.
   This is a small MPI / OpenMP program which will print a small message for each MPI rank and each OpenMP thread running on the MPI rank.
   We use it here to show how the different Slurm parameters `--nodes`, `--tasks-per-node`, and `--cpus-per-task` is used to control resources of a batch job.
  
   Compile the small test program.
   
   ```
      $ ml gcc/8.2.0
      $ ml openmpi/4.0.1
      $ mpicc -fopenmp -o hello_world.x main.c
   ```

   You can also try a different setup of compiler and MPI if you so desire (using an Intel compiler you might have to change `-fopenmp` to `-qopenmp`).

2) **Run on your own computer or on the frontend (even though this forbidden!)**

   Try to run the program locally:

   ```
      $ mpirun -n 2 ./hello_world.x
   ```

   What does it print? Does the output meet your expectations?

   Try running with a different number of MPI ranks:

   ```
      $ mpirun -n 3 ./hello_world.x
   ```
   
   We also can set the number of OpenMP threads the program uses by setting the variable `OMP_NUM_THREADS`.
   Try to do that and run the program again:

   ```
      $ export OMP_NUM_THREADS=5
      $ mpirun -n 2 ./hello_world.x
   ```
      
   We will now try to compile a second version of the program. 
   This is just a little different in that it keep all threads and ranks busy indefinetely,
   allowing us to do some diagnostics on the how the program is running.
   Compile and run the program in the following way:
   ```
      $ mpicc -DBUSY_WAIT -fopenmp -o hello_world_busy.x main.c
      $ export OMP_NUM_THREADS=2
      $ mpirun -n 2 ./hello_world_busy.x &
   ```
   
   After this inspect the program with the `top` command:
   ```
      $ top
   ```
   Is the resource use of the program what you expected (2 processes each running 2 threads)? 
   Look at number of separate processes and `%CPU` usage.
   You can try different options for number of threads and ranks, but remember to kill the old process before starting a new!
   To kill the process, foreground it with:
   ```
      $ fg
   ```
   and when foregrounded kill the process by hitting `Ctrl-D`,
   or kill the process with the `kill` command using the process id which can be seen on the left side of the `top` output 
   (just chose one of the MPI ranks, the rest will crash when one rank is lost).
   ```
      $ kill -9 <process id>
   ```

3) **Submitting to Slurm**
   
   First have a quick look at the submit file.
   It is located in the file `submit.sh`. 
   Open it using your favorite terminal editor.

   Now we will look at how the program will run when submitted to the Slurm queue,
   and run with the `srun` command.
   We will see how the program is run differently with different setups for `--nodes`, `--tasks-per-node`, and `--cpus-per-task`.

   Submit the job to the workshop reservation by calling `sbatch` with the script as an argument:
      
      ```
         $ sbatch --reservation=<reservation> submit.sh
      ```

   To see your job in the queuing system type
      
      ```
         $ mj
      ```

   When the state is marked as `C`, it means the job has Completed.
   `P` means Pending (waiting to start), and `R` means Running.
   After the job has run, you will see a file called `slurm-<jobid>.out`. 
   This file collects all output written to stdout or stderr
   from the slurm job. 
   
   Open the output file and notice that our two MPI ranks where run on
   the same node. 
   Try changing the number of nodes by changing the line:  
   
      `#SBATCH --nodes 1` ->  `#SBATCH --nodes 2`

   Resubmit your slurm script. Open the new slurm output file. Here you can see
   that the job has now run on two different nodes. Also notice that we didn't change
   any arguments to `srun`, we only changed the number of nodes at the top of our script
   and `srun` automatically adjusted.

   Try different setups for `--nodes`, `--tasks-per-node`, and `--cpus-per-task`,
   and make sure the output matches your expectations.
