#! /bin/bash

# The top part (with all the weird #SBATCH stuff) contains all the slurm settings for the job,
# e.g. where to run the program and which resources to use.
#SBATCH -J slurm_test_job        # Job name (you can try to change it).
#SBATCH --partition qgpu         # Which partition to use.
#SBATCH --time=1:00:00           # Time limit.
#SBATCH --nodes 1                # The number of nodes to allocate.
#SBATCH --ntasks-per-node 2      # The number of tasks to run on each node.
#SBATCH --cpus-per-task 1        # The number of cpus per task (threading).

# Set number of OMP threads to number of CPUs per task
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK

# We use srun to run our application.
# This is a special slurm wrapper that will wrap mpirun, and make sure that the application is run correctly.
# Srun will also make sure that the application will use all the resources you requested.
srun $SLURM_SUBMIT_DIR/hello_world.x 
