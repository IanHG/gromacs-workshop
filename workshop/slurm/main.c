#include <stdlib.h>
#include <stdio.h>
#include <unistd.h> /* for gethostname       */
#include <mpi.h>    /* Special mpi header    */
#include <omp.h>    /* Special openmp header */

/* 
 * Some preprocessor magic
 */
#define xstr(s) str(s)
#define str(s) #s

#if defined __INTEL_COMPILER
   #define COMPILER intel-__INTEL_COMPILER
#elif defined __clang__
   #define COMPILER clang-__clang_major__.__clang_minor__.__clang_patchlevel__
#elif defined __GNUC__
   #define COMPILER gcc-__GNUC__.__GNUC_MINOR__.__GNUC_PATCHLEVEL__
#elif defined __PGI
   #define COMPILER pgi-__PGIC__.__PGIC_MINOR__.__PGIC_PATCHLEVEL__
#endif

/* 
 * Actual program
 */
int main(int argc, char* argv[])
{
   /* Initialize MPI */
   MPI_Init(&argc, &argv);
   
   /* Get number of ranks and get this process' rank */
   int size;
   int rank;
   MPI_Comm_size(MPI_COMM_WORLD, &size);
   MPI_Comm_rank(MPI_COMM_WORLD, &rank);

   /* Get hostname */
   char hostname[512];
   gethostname(hostname, 512);
   
   /* Loop over ranks so we can "pretty print" (see comment for MPI_Barrier() below) */
   for( int irank = 0; irank < size; ++irank)
   {
      if(irank == rank)
      {
         /* Print "Hello" from MPI rank */
         printf("=================== Rank %i ===================\n", rank);
         printf("Hello World from rank %i. Compiler : %s.\n", rank, xstr(COMPILER));
         printf("Rank %i is running on host : %s\n", rank, hostname);
         
         int nthreads, tid;

         /* Fork a team of OMP threads giving them their own copies of variables */
         #pragma omp parallel private(nthreads, tid)
         {
            /* Obtain thread number */
            tid = omp_get_thread_num();
            /* Only master thread does this */
            if (tid == 0) 
            {
               nthreads = omp_get_num_threads();
               printf("Number of threads = %d\n", nthreads);
            }
            /* print special thread message from each OMP thread */
            printf("Hello World from thread = %d\n", tid);
            
            /* If compiled in BUSY_WAIT mode, we... well, we busy wait... */
            #ifdef BUSY_WAIT
            while(1)
            {
            }
            #endif /* BUSY_ WAIT */

         } /* All threads join master thread and disband */
         printf("=================== Rank %i ===================\n", rank);
      }
      
      /* If not compiled in BUSY_WAIT mode, we put a barrier for the MPI ranks here, to get a prettier print-out. */
      /* With the Barrier here, we make sure that a rank does start to print before the rank before it has finished writing */
      /* this way we do not get the output from different ranks mangled (the Barrier is of course highly inefficient, so don't do this is a hot spot...). */
      #ifndef BUSY_WAIT
      MPI_Barrier(MPI_COMM_WORLD);
      #endif /* ! BUSY_WAIT */
   }
   
   /* Finalize MPI */
   MPI_Finalize();

   return 0;
}
