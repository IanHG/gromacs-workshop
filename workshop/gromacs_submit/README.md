Gromacs submission example
======

In this exercise we learn how to submit a Gromacs job to the slurm queue system.

For reference output files look in the `data` folder of the main directory of this repository.

1) **Get the example system**

   Download and unpack the example system used for the Workshop.

   To download type in
   ```
      $ ./download MEM
   ```
   or, alternatively you can download by directly calling wget
   ```
      $ wget https://www.mpibpc.mpg.de/15101317/benchMEM.zip
   ```

   Unpack the example system
   ```
      $ unzip benchMEM.zip
   ```

   If you run these commands in the `slurm` folder everything should be setup.

2) **Single node jobs**

    Look at the script `gromacs_submit.sh`. This is a script for submitting Gromacs jobs.
    For single node jobs we will run PP tasks and a single PME task on GPU.

    Look at the script and determine whether it is currently setup to run a half node or a full node job 
    (Hint: look at the Slurm header).

    a) *Submit the job*

      Submit the job by running
      ```
         $ sbatch --reservation=<reservation> gromacs_submit.sh
      ```

      The output will be copied back to a folder called `job_<jobname>_<jobid>`.
      Look at the output using your favorite editor.
      
      Find out how tasks are mapped to the GPUs (search for `GPUs auto-selected`).
      Does the mapping correspond to your expectations?

      Look at the bottom and note the `ns/day` throughput.

      *Extra*: Search for `Hardware detected` in the Gromacs output file. 
               This gives a nice detailed overview of node hardware.
               Note, that Gromacs can detect a GPU without actually running any tasks on it.
               You will need to look at the task mapping, to see which GPUs Gromacs is actually using.

   b) *Run a full node job*

      Currently `gromacs_submit.sh` will submit a half-node job, using 1 CPU and 1 GPU.
      To change it to a full node job, either change the following lines of the submit script:

         ```
          #SBATCH --ntasks-per-node=6
          #SBATCH --cpus-per-task=6
          #SBATCH --mem=300G
          ...
          #SBATCH --gres=gpu:v100:2
         ```

      or pass the arguments directly to `sbatch` when submitting:

      ```
         $ sbatch --ntasks-per-node=6 --cpus-per-task=6 --mem=300G --gres=gpu:v100:2 gromacs_submit.sh
      ```

      Change the Slurm header or pass arguments to `sbatch` such that the script will use a full node.

      Try different settings for `ntasks-per-node` and `ncpus-per-task`.
      Remember that `ntasks-per-node * ncpus-per-task` should not be more than 36.

      What happens with the `ns/day` troughput.

      *Extra*: Try changing PME tasks to run on CPU instead of GPU. PME tasks can be forced to CPU by passing `--pme cpu` to `gromacs_submit.sh`

               ```
                  $ sbatch --ntasks-per-node=8 --cpus-per-task=4 --mem=300G --gres=gpu:v100:2 gromacs_submit.sh --pme cpu
               ```

               What happens to the `ns/day` throughput?


3) **Multi node jobs**

   Multinode jobs we run PP tasks on GPU and PME tasks on CPU in separate tasks, as this will likely give the best scaleability.

   a) *Submit the job*

      Submit the job by running
      ```
         $ sbatch --reservation=<reservation> --nnodes=2 --ntasks-per-node=36 --cpus-per-task=1 gromacs_submit.sh
      ```

      The output will be copied back to a folder called `job_<jobname>_<jobid>`.
      Look at the output on your favorite editor.
      
      Find out how tasks are mapped to the GPUs (search for `GPUs auto-selected`).
      Does the mapping correspond to your expectations?

      Look at the bottom and note the `ns/day` throughput.
      
      Try to run on a single node. 
      Here we need to force the script ot use the standard MPI version of Gromacs, and for the script to run PME on cpu, for a fair comparison with the 2 node job.
      ```
         $ sbatch --reservation=<reservation> --nnodes=1 --ntasks-per-node=36 --cpus-per-task=1 gromacs_submit.sh --force-no-tmpi --pme cpu
      ```
      
      Look at the bottom and note the `ns/day` throughput. Is this what you expected?

   b) *Change it up*

      Try change number of tasks and numbers of cpus per task. 
      You could e.g. set `--ntasks-per-node=8` and `--ncpus-per-task=4` (which would use 32 cores total).
      It is probably also a good idea to have an even number of tasks (for even distribution onto the two CPU sockets),
      but if you feel like doing something crazy try an uneven number and see if you can get it to run.

      You can also try to change the relative number of NPME tasks compared to the total number of tasks.
      This can be done by modifying pasing `--pme-fraction <N>` to `gromacs_submit.sh.
      To use a `1/6`'th of the tasks for PME submit with:
      ```
         $ sbatch --nnodes=2 --ntasks-per-node=36 --cpus-per-task=1 gromacs_submit.sh --force-no-tmpi --pme cpu --pme-fraction 6
      ```

      For each input you try, find the mapping of GPU tasks and make sure it fits your expectation, 
      and look at the `ns/day` throughput (is it higher/lower than waht you've seen before).

4) **RIB and PEP**

   You can also try to run on the RIB and PEP examples. To download:

   ```
      $ ./download.sh RIB
      $ unzip benchRIB.zip
   ```
   
   ```
      $ ./download.sh PEP
      $ unzip benchPEP.zip
   ```

   To use one of these inputs submit with
   ```
      $ sbatch gromacs_submit.sh --input benchRIB.tpr
   ```


   To reduce runtime for these examples change the following lines in `gromacs_submit.sh`:
   ```
      STEPSMEM=20000
      RESETMEM=15000
   ```
   to
   ```
      STEPSMEM=10000
      RESETMEM=8000
   ```
