#!/bin/bash

FILE=$1

if [ $FILE = "MEM" ]; then
   echo "HERE"
   wget https://www.mpibpc.mpg.de/15101317/benchMEM.zip
   unzip benchMEM.zip
elif [ $FILE = "RIB" ]; then
   wget https://www.mpibpc.mpg.de/15101328/benchRIB.zip
   unzip benchRIB.zip
elif [ $FILE = "PEP" ]; then
   wget https://www.mpibpc.mpg.de/15615646/benchPEP.zip
   unzip benchPEP.zip
fi
