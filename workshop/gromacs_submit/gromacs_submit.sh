#!/bin/bash

#SBATCH --job-name=gromacs
#SBATCH --partition=qgpu
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=18
#SBATCH --mem=150G
#SBATCH --time=01:00:00
#SBATCH --gres=gpu:v100:1

#==========================================================
# Default values
#==========================================================
INPFILE=benchMEM.tpr       # Input file
DLB=no                     # Turn off Dynamic Load Balancer (seems to be faster in most cases)
PME_FRACTION=2
if [ "$SLURM_JOB_NUM_NODES" -eq "1" ]; then
   PME=gpu # When running singlenode we run PME tasks on GPU 
else
   PME=cpu # When running multinode we run PME tasks on CPU 
fi
FORCE_NO_TMPI=0

#==========================================================
# Helper functions
#==========================================================
func.getGpuStr() {
   if [ "$SLURM_NTASKS_PER_NODE" -gt "1" ]; then
      echo "01"    # For multi-task we need both GPU's
   else
      echo "0"     # For single task we always set "0"
   fi
}

func.getNpme() {
   if [ "$SLURM_NTASKS_PER_NODE" -eq "1" ]; then
      LOCAL_NPME=0
   elif [ "$PME" = "gpu" ]; then
      LOCAL_NPME=1
   elif [ "$SLURM_NTASKS_PER_NODE" -eq "2" ]; then
      LOCAL_NPME=0
   else
      LOCAL_NPME=`echo "$SLURM_JOB_NUM_NODES * $SLURM_NTASKS_PER_NODE / $PME_FRACTION" | bc`
   fi

   echo "${LOCAL_NPME}"
}

func.checkAffinity() {
   if [ "$SLURM_NTASKS_PER_NODE" -eq "1" ]; then
      PROCESS_AFFINITY=$1
      if [ "${PROCESS_AFFINITY}" = "0-17" ]; then
         CPUSOCKET=0
      elif [ "${PROCESS_AFFINITY}" = "18-35" ]; then
         CPUSOCKET=1
      else
         CPUSOCKET=-1
      fi

      if [ ! "${CUDA_VISIBLE_DEVICES}" = "${CPUSOCKET}" ]; then
         echo "#!!!!!! WARNING !!! WARNING !!! WARNING !!!!!!#"
         echo "CPU socket and GPU device are not directly connected"
         echo "You might see performance degradation due to NUMA effects."
         echo "#!!!!!! WARNING !!! WARNING !!! WARNING !!!!!!#"
      else
         echo "Process CPU affinity: OK."
      fi

   else
      echo "*NOT* checking process CPU affinity."
   fi
}

#==========================================================
# Functions for parsing input parameters
#==========================================================
func.usage() {
   printf "Usage: $0 [options] example\n"
   printf "Options:\n"
   printf "   -h,--help             Display this message.\n"
   printf "   --dlb <dlb>           Set 'dlb' yes/no (default: 'no').\n"
   printf "   --pme <pme>           Force PME tasks to 'cpu' or 'gpu'..\n"
   printf "   --pme-fraction <N>    Set the fraction of separate PME processes.\n"
   printf "   --force-no-tmpi       Force use of standard MPI version of Gromacs.\n"
   printf "   --input <file>        Provide input file.\n"
}

func.parseCommandLine() {
   while getopts "vhq:t:r:-:" opt; do
      case $opt in
         ## Handle single character options.
         h)
            func.usage; exit
         ;;
         # Handle multicharacter options.
         -) case ${OPTARG} in
            help)
               func.usage; exit
            ;;
            dlb)
               DLB="${!OPTIND}"
               OPTIND=$(( $OPTIND + 1 ))  # increment opt index (OPTIND)
            ;;
            pme)
               PME="${!OPTIND}"
               OPTIND=$(( $OPTIND + 1 ))  # increment opt index (OPTIND)
            ;;
            pme-fraction)
               PME_FRACTION="${!OPTIND}"
               OPTIND=$(( $OPTIND + 1 ))  # increment opt index (OPTIND)
            ;;
            force-no-tmpi)
               FORCE_NO_TMPI=1
            ;;
            input)
               INPFILE="${!OPTIND}"
               OPTIND=$(( $OPTIND + 1 ))  # increment opt index (OPTIND)
            ;;
            esac
         ;;
         # Handle unknown
         ?)
            echo "Argument '${opt}' not known"
         ;;
      esac
   done
}

#==========================================================
# Setup 
#==========================================================
# Print nice job header
echo "#========= Job started  at `date` ==========#"

# Parse input parameters
echo "Parsing command-line input: $@"
func.parseCommandLine "$@"

CPUSET=`cat /sys/fs/cgroup/cpuset/slurm/uid_$(id -u)/job_${SLURM_JOBID}/cpuset.cpus`

echo ""
echo "#===========================================#"
echo "hostname             = "`hostname`
echo "Slurm nodelist       = "${SLURM_JOB_NODELIST}
echo "CUDA_VISIBLE_DEVICES = "${CUDA_VISIBLE_DEVICES}
echo "CPUSET               = "${CPUSET}
echo "#===========================================#"
echo ""

# Check CPU 
func.checkAffinity ${CPUSET}

# Load Gromacs module
source /comm/specialstacks/gromacs-volta/bin/modules.sh --force
if [ "$SLURM_JOB_NUM_NODES" -eq "1" ] && [ ! "$FORCE_NO_TMPI" -eq "1" ]; then
   ml gromacs-tmpi-gcc-8.2.0-openmpi-4.0.1-cuda-10.1/2018.6
   MDRUN="gmx mdrun -ntmpi $SLURM_NTASKS_PER_NODE"
else
   ml gromacs-gcc-8.2.0-openmpi-4.0.1-cuda-10.1/2018.6
   MDRUN="srun gmx_mpi mdrun"
fi

# Create scratch directory 
SCRATCH=/scratch/$SLURM_JOBID
cd $SCRATCH

# Copy input file to node
sbcast $SLURM_SUBMIT_DIR/$INPFILE $SCRATCH/$INPFILE

# Unbind default OMP settings to try and let Gromacs decide how/where to spawn threads
unset OMP_PLACES
unset OMP_PROC_BIND
unset KMP_AFFINITY

#==========================================================
# Run Gromacs job
#==========================================================
echo ""
echo "#================ GROMACS ==================#"
NTOMP=$SLURM_CPUS_PER_TASK # Set number of OpenMP threads
NPME=$(func.getNpme)       # Set number of separate PME tasks. For half node jobs this should ALWAYS be 0.
STEPSMEM=20000
RESETMEM=15000
#STEPSMEM=10000
#RESETMEM=8000
GPUSTR=$(func.getGpuStr)   
DDORDER=interleave         # Interleave PP/PME tasks (I think this is the default)
PIN=on                     # Pin threads to physical cores, such that threads cannot move between cores or sockets (should provide small speed-ups).
GCOM=100                   # Rate of communication (not completely sure what it does, but I find a value of 100 gives a nice performace)

# Setup Gromacs command line options
gromacs_args=()
gromacs_args+=("-pin $PIN")
gromacs_args+=("-ntomp $NTOMP")
gromacs_args+=("-npme $NPME")
gromacs_args+=("-s $INPFILE")
gromacs_args+=("-cpt 1440")
gromacs_args+=("-nsteps $STEPSMEM")
gromacs_args+=("-resetstep $RESETMEM")
gromacs_args+=("-v")
gromacs_args+=("-gcom $GCOM")
gromacs_args+=("-noconfout")
gromacs_args+=("-nb gpu")
gromacs_args+=("-pme $PME")
gromacs_args+=("-dlb $DLB")
gromacs_args+=("-gpu_id $GPUSTR")
gromacs_args+=("--ddorder $DDORDER")

# Call Gromacs
$MDRUN ${gromacs_args[@]} &

# Get PID and check gromacs CPU affinity
PID=$!
TASKSET=`taskset -pc ${PID}`
TASKSETCPUS=`echo "${TASKSET}" | cut -d ":" -f 2 | sed -e 's/^[[:space:]]*//'`

# Wait for Gromacs to finish
wait ${PID}

echo "#================ GROMACS ==================#"

#==========================================================
# Finalize job
#==========================================================
echo ""
echo "#===========================================#"
echo "PID                    = "${PID}
echo "TASKSET                = "${TASKSET}
echo "#===========================================#"
echo ""


# Copy back results
echo "Copying back files:"
ls $SCRATCH

JOBDIR=job_$SLURM_JOB_NAME\_$SLURM_JOBID
mkdir $SLURM_SUBMIT_DIR/$JOBDIR
cp $SCRATCH/* $SLURM_SUBMIT_DIR/$JOBDIR/.    # You can do an 'rsync' here instead

# Print nice job footer
echo "#========= Job finished at `date` ==========#"
