#!/bin/bash

# Check for Instruction set
flags=`lscpu | grep -i flags`
check_flags=$?

if [ ! "$check_flags" = "0" ]; then
   flags=`cat /proc/cpuinfo | grep -i -m 1 flags`
fi

echo $flags | grep -i "avx512" &> /dev/null
check_avx512=$?
echo $flags | grep -i "avx2" &> /dev/null
check_avx2=$?
echo $flags | grep -i "sse4.2" &> /dev/null
check_sse4_2=$?

# If based on result
if [ "$check_avx512" = "0" ];
then
   # Here you can load AVX512 version of gromacs
   echo "AVX512"
elif [ "$check_avx2" = "0" ];
then
   # Here you can load AVX2 version of gromacs
   echo "AVX2"
elif [ "$check_sse4_2" = "0" ];
then
   # Here you can load SSE4.2 version of gromacs
   echo "SSE4.2"
else
   # Here you can load whatever gimp version of gromacs
   echo "Unknown instruction set"
fi
