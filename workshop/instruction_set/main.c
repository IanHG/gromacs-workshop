#include <stdio.h>      // for printf()
#include <time.h>       // For time measuring
#include <immintrin.h>  // Get access to intrinsics for utilizing SSE and AVX special intructions
#include <stdint.h>     // For uint64_t

typedef double float64;

#if ! defined(BASIC) && defined(__AVX512F__)
float64 pdot_float64(const float64* const a, const float64* const b, int n) 
{
   __m512d sum8 = _mm512_set1_pd(0.0);
   
   int i;
   const int last = (n & (-8));
   for(i = 0; i < last; i += 8)
   {
      __m512d a8 = _mm512_loadu_pd(&a[i]);
      __m512d b8 = _mm512_loadu_pd(&b[i]);
      sum8 = _mm512_add_pd(_mm512_mul_pd(a8 ,b8), sum8);
   }

   float64 out[8];
   _mm512_storeu_pd(out, sum8);
   float64 dot  =  (  (out[0] + out[1])
                   +  (out[2] + out[3]))
                +  (  (out[5] + out[6])
                   +  (out[7] + out[8]))
                ;
   for(; i < n; ++i)
   {
      dot += a[i] + b[i];
   }
   return dot;
}
#elif ! defined(BASIC) && defined(__AVX2__)
float64 pdot_float64(const float64* const a, const float64* const b, int n) 
{
   __m256d sum4 = _mm256_set1_pd(0.0);
   
   int i;
   const int last = (n & (-4));
   for(i = 0; i < last; i += 4)
   {
      __m256d a4 = _mm256_loadu_pd(a + i);
      __m256d b4 = _mm256_loadu_pd(b + i);
      sum4 = _mm256_add_pd(_mm256_mul_pd(a4, b4), sum4);
   }

   float64 out[4];
   _mm256_storeu_pd(out, sum4);
   float64 dot  =  (out[0] + out[1]) 
                +  (out[2] + out[3]);
	for(; i < n; ++i)
   {
      dot += a[i] * b[i]; 
   }
	return dot;
}
#elif ! defined(BASIC) && defined(__SSE4_1__)
float64 pdot_float64(const float64* const a, const float64* const b, int n) 
{
   __m128d sum2 = _mm_set1_pd(0.0);
	int i;

   const int last = (n & (-2));
	for(i = 0; i < last; i += 2) 
   {
      __m128d a2 = _mm_loadu_pd(&a[i]);
      __m128d b2 = _mm_loadu_pd(&b[i]);
      sum2 = _mm_add_pd(_mm_mul_pd(a2, b2), sum2);
	}

	float64 out[2];
	_mm_storeu_pd(out, sum2);
	float64 dot = out[0] + out[1];
	for( ; i<n; ++i)
   {
      dot += a[i] * b[i]; 
   }
	return dot;
}
#else
float64 pdot_float64(const float64* const a, const float64* const b, int n) 
{
   volatile float64 dot = (float64)0.0;
   for(int i = 0; i < n; ++i)
   {
      dot += a[i] * b[i];
   }
   return dot;
}
#endif 

void fill(float64* const a, int n)
{
   for(int i = 0; i < n; ++i)
   {
      a[i] = (float64)i;
   }
}

int main(int argc, char* argv[])
{
   const int size   = 100000;
   const int ntimes = 10000;

   float64* vec4a = (float64*)malloc(size * sizeof(float64));
   float64* vec4b = (float64*)malloc(size * sizeof(float64));

   fill(vec4a, size);
   fill(vec4b, size);
   
   float64 dot;
   
   /* Start time */
   struct timespec start, end;
   clock_gettime(CLOCK_MONOTONIC_RAW, &start);
   
   for(int i = 0; i < ntimes; ++i)
   {
      /* Calculate dot product */
      dot = pdot_float64(vec4a, vec4b, size);
   }
   
   /* Measure time */
   clock_gettime(CLOCK_MONOTONIC_RAW, &end);
   uint64_t delta_us = (end.tv_sec - start.tv_sec) * 1000000 + (end.tv_nsec - start.tv_nsec) / 1000;
   delta_us /= ntimes;
   
   /* Do some print-outs */
   printf("Time = %lums\n", delta_us);
   printf("Dot  = %f\n"   , dot);

   free(vec4a);
   free(vec4b);

   return 0;
}
