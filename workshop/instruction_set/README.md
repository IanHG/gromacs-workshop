SSE4.2 / AVX2 / AVX512
======

1) **How to check which instruction set is available**
   
   Checking which instruction set is available can be done in two ways, 
   either using `lscpu` or by looking in the file `/proc/cpuinfo`.

   ```
      $ lscpu | grep -i flags
   ```

   ```
      $ cat /proc/cpuinfo | grep -i -m 1 flags
   ```

   The easiest is just to use `lscpu`, but on some systems this will not return CPU `flags`,
   and one has to look at `/proc/cpuinfo`.

   Try both commands.
   
   Using the output of the above commands one can also look for specific instruction sets.
   ```
      $ lscpu | grep -i flags | grep avx2
   ```

2) **Script for checking availability of instruction sets**

   Look at the `check_instruction_set.sh` script.
   Try to run the script on some different machines.
   Manually verify that i works, e.g. by inspecting output from `lscpu`.

3) **Compiling for specific instruction sets**

   Have a look at `main.c`. It contains a program to do dot products of two vectors.
   It contains special code using some of the special instructions from either SSE4.1, AVX2, or AVX512.
   We will now try to compile the program using the different instruction sets and see how they run.
   
   To compile for SSE4.1, AVX2, or AVX512 do:
   ```
      $ gcc -msse4.1 main.c
   ```
   ```
      $ gcc -mavx2 main.c
   ```
   ```
      $ gcc -mavx512 main.c
   ```

   To compile a general version not using any special intrinsics compile like this:
   ```
      $ gcc -DBASIC main.c
   ```
   
   Running the program will print the result of the dot product 
   as well as the time it took on average pr. dot product that was calculated.
   To run the program do:
   ```
      $ ./a.out
      Time = 147ms
      Dot  = 333328333350000.000000
   ```

   Compile and the run the program for SSE4.1, AVX2, and AVX512. What do you see? Do you get a speed-up using the newer instruction sets?

   If you see
   ```
      $ ./a.out
      Illegal instruction (core dumped)
   ```
   it means that you tried to use an executable compiled compiled for an instruction set that the CPU does not support,
   e.g. you might be trying to run an AVX512 executable on a machines taht only supports up to AVX2.

   Try compiling and running on different machines (see if you can find some AVX512 machines and test on).
   
   Will AVX512 machines run all the executables no matter how they were compiled?
