Slurm Tools
======
   
   Here are located some scripts for inspection of cluster usage.

1) **`lsload`**

   `lsload` will show CPU usage of all Slurm user groups relative to max CPU run-minutes.

   ```
      $ lsload
   ```

2) **`lsgpu`**

   `lsgpu` will show GPU usage of all Slurm user groups.

   ```
      $ lsgpu
   ```

3) **`sqw`**

   `sqw` will show Slurm job priority ranking.

   ```
      $ sqw
   ```

