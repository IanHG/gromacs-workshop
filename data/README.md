Reference Data output files
======

   Here are located reference output files, from which results for slides were gathered.

   Nomenclature:
      
      *_1gpu       :  Half-node job, nnodes=1, ntasks-per-node=1, cpus-per-task=18, pme on GPU
      *_2gpu       :  Full-node job, nnodes=1, ntasks-per-node=2, cpus-per-task=18, pme on CPU
      *_2gpu_tweak :  Full-node job, nnodes=1, ntasks-per-node=6, cpus-per-task=6,  pme on GPU
      
      *_1multi     :  Multi-node job, nnodes=1, ntasks-per-node=36, cpus-per-task=1,  pme on CPU
      *_2multi     :  Multi-node job, nnodes=1, ntasks-per-node=36, cpus-per-task=1,  pme on CPU
